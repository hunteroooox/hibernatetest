/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.scalda.pasimo.model.EmployeeManagement;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 *
 * @author jeroe
 */
@Entity
@Table(name = "student")
public class Student implements Comparable<Student>, Serializable { //deze class word vervangen met die van jos/elroy als die van hun klaar zijn

    @Id
    private String name;
    private int cardID;
    private String CouchGroupName;

    public Student() {
    }

    public Student(String name, int cardID) {
        this.name = name;
        this.cardID = cardID;
    }

    public Student(String name, int cardID, String CouchGroupName) {
        this.name = name;
        this.cardID = cardID;
        this.CouchGroupName = CouchGroupName;
    }

//<editor-fold defaultstate="collapsed" desc="getters and setters">
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCardID() {
        return cardID;
    }

    public void setCardID(int cardID) {
        this.cardID = cardID;
    }

    public String getCouchGroupName() {
        return CouchGroupName;
    }

    public void setCouchGroupName(String CouchGroupName) {
        this.CouchGroupName = CouchGroupName;
    }

//</editor-fold>
    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", cardID=" + cardID + '}';
    }

    @Override
    public int compareTo(Student o) {
        return name.compareTo(o.getName());
    }

}
