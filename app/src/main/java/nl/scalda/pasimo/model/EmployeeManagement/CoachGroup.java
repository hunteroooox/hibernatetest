package nl.scalda.pasimo.model.EmployeeManagement;

import java.io.Serializable;
import java.util.TreeSet;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author jeroe
 */
@Entity
@Table(name = "coachgroup")
public class CoachGroup implements Comparable<CoachGroup>, Serializable {

    private String name;
    @Id
    private String abbr;
    private TreeSet<Student> students = new TreeSet<>();

    public CoachGroup() {
    }

    public void addStudent(Student s) {
        this.students.add(s);
    }

    public void deleteLessonGroup(Student s) {
        students.remove(s);
    }

//<editor-fold defaultstate="collapsed" desc="getters and setters">    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public TreeSet<Student> getStudents() {
        return students;
    }

    public void setStudents(TreeSet<Student> students) {
        this.students = students;
    }
//</editor-fold>

    @Override
    public int compareTo(CoachGroup o) {
        return name.compareTo(o.getName());
    }
}
