package nl.scalda.pasimo.main;

import nl.scalda.pasimo.datalayer.HibernateTest;
import nl.scalda.pasimo.model.EmployeeManagement.CoachGroup;
import nl.scalda.pasimo.model.EmployeeManagement.Student;

public class Main {

    public static void main(String[] args) throws Exception {

        CoachGroup a1 = new CoachGroup();
        a1.setName("Applicatie Ontwikkeling");
        a1.setAbbr("A1");
        
        CoachGroup a2 = new CoachGroup();
        a2.setName("Applicatie Ontwikkeling");
        a2.setAbbr("A2");
        
        Student jeroen = new Student("Jeroen", 203313);
        Student armando = new Student("Armando", 203512);
        Student niek = new Student("Niek", 205657);
        Student kevin = new Student("Kevin", 200666);

        HibernateTest ht = new HibernateTest();
//        ht.saveStudent("Jeroen", 203313, a1.getAbbr());
        ht.getCoachGroup();

    }
}
