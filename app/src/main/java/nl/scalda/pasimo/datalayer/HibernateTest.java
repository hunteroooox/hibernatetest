package nl.scalda.pasimo.datalayer;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import nl.scalda.pasimo.model.EmployeeManagement.CoachGroup;
import nl.scalda.pasimo.model.EmployeeManagement.Student;

/**
 *
 * @author jeroe
 */
public class HibernateTest {

    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");

    public void saveStudent(String name, int cardID, String cgn) throws Exception {
        Student student = new Student();

        try {
            EntityManager em = entityManagerFactory.createEntityManager();
            em.getTransaction().begin();
            student.setName(name);
            student.setCardID(cardID);
            student.setCouchGroupName(cgn);
            em.persist(student);
            em.flush();
            em.getTransaction().commit();
            em.close();
        } catch (Exception w) {
            w.printStackTrace();
        }
    }

    public void getCoachGroup() {
        try {
            EntityManager em = entityManagerFactory.createEntityManager();
            em.getTransaction().begin();
            Query q = em.createQuery("select cg from CoachGroup cg", CoachGroup.class);
            List cg = q.getResultList();
            em.getTransaction().commit();
            em.close();
            System.out.println("list sice: " + cg.size());
            for (CoachGroup ccg : (List<CoachGroup>) cg) {
                System.out.println("CoachGroup: name: " + ccg.getName() + " Abbr: " + ccg.getAbbr());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
