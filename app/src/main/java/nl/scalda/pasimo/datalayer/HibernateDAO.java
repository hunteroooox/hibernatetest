/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.scalda.pasimo.datalayer;

import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import nl.scalda.pasimo.model.EmployeeManagement.CoachGroup;
import nl.scalda.pasimo.model.EmployeeManagement.Student;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author jeroe
 */
public class HibernateDAO {

    private static EntityManager entityManager;

    /* Method to CREATE an employee in the database */
    public void addStudent(Student student, CoachGroup coachgroup) {
            student.setCouchGroupName(coachgroup.getName());
        entityManager.persist(student);
//        Transaction tx = null;
//        Integer employeeID = null;
//        try {
//            tx = session.beginTransaction();
//            student.setCouchGroupName(coachgroup.getName());
//            employeeID = (Integer) session.save(student);
//            tx.commit();
//        } catch (HibernateException e) {
//            if (tx != null) {
//                tx.rollback();
//            }
//            e.printStackTrace();
//        } finally {
//            session.close();
//        }
//        return employeeID;
    }

//    /* Method to  READ all the employees */
//    public void listStudent() {
//        Session session = entityManager.openSession();
//        Transaction tx = null;
//        try {
//            tx = session.beginTransaction();
//            List students = session.createQuery("FROM student").list();
//            for (Iterator iterator
//                    = students.iterator(); iterator.hasNext();) {
//                Student student = (Student) iterator.next();
//                System.out.print("Name: " + student.getName());
//                System.out.print("Ovnummer: " + student.getCardID());
//            }
//            tx.commit();
//        } catch (HibernateException e) {
//            if (tx != null) {
//                tx.rollback();
//            }
//            e.printStackTrace();
//        } finally {
//            session.close();
//        }
//    }
//
//    public Integer addCoachGroup(CoachGroup coachgroup) {
//        Session session = entityManager.openSession();
////        Transaction tx = null;
//        Integer employeeID = null;
//        try {
////            tx = session.beginTransaction();
//session.getEntityManagerFactory().createEntityManager().persist(session);
//            employeeID = (Integer) session.save(coachgroup);
////            tx.commit();
//        } catch (HibernateException e) {
////            if (tx != null) {
////                tx.rollback();
////            }
//            e.printStackTrace();
//        } finally {
//            session.close();
//        }
//        return employeeID;
//    }
//
//    public void listCoachGroup() {
//        Session session = entityManager.openSession();
//        Transaction tx = null;
//        try {
//            tx = session.beginTransaction();
//            List students = session.createQuery("FROM coachgroup").list();
//            for (Iterator iterator
//                    = students.iterator(); iterator.hasNext();) {
//                CoachGroup coachgroup = (CoachGroup) iterator.next();
//                System.out.print("Name: " + coachgroup.getName());
//                System.out.print("Abbr: " + coachgroup.getAbbr());
//            }
//            tx.commit();
//        } catch (HibernateException e) {
//            if (tx != null) {
//                tx.rollback();
//            }
//            e.printStackTrace();
//        } finally {
//            session.close();
//        }
//    }

//
//    /* Method to UPDATE salary for an employee */
//    public void updateEmployee(Integer EmployeeID, int cardID) {
//        Session session = factory.openSession();
//        Transaction tx = null;
//        try {
//            tx = session.beginTransaction();
//            Student student = (Student) session.get(Student.class, EmployeeID);
//            student.setCardID(cardID);
//            session.update(student);
//            tx.commit();
//        } catch (HibernateException e) {
//            if (tx != null) {
//                tx.rollback();
//            }
//            e.printStackTrace();
//        } finally {
//            session.close();
//        }
//    }
//
//    /* Method to DELETE an employee from the records */
//    public void deleteEmployee(Integer EmployeeID) {
//        Session session = factory.openSession();
//        Transaction tx = null;
//        try {
//            tx = session.beginTransaction();
//            Employee employee = (Employee) session.get(Employee.class, EmployeeID);
//            session.delete(employee);
//            tx.commit();
//        } catch (HibernateException e) {
//            if (tx != null) {
//                tx.rollback();
//            }
//            e.printStackTrace();
//        } finally {
//            session.close();
//        }
//    }
    public static EntityManager getFactory() {
        return entityManager;
    }

    public static void setFactory(EntityManager factory) {
        HibernateDAO.entityManager = factory;
    }

}
